host="dc-medical-file-mysql"
port="3306"
while ! nc -z "$host" "$port"; do
    >&2 echo "Mysql  is unavailable - sleeping";
    sleep 3;
done
>&2 echo "$host is up - executing command"
echo $host $port