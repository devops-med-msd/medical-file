FROM adoptopenjdk/openjdk11:latest
COPY target/*.jar medical-file.jar
#COPY entrypoint.sh /usr/bin/entrypoint.sh
#RUN chmod u+x /usr/bin/entrypoint.sh
#RUN apt-get update && apt-get install -y netcat
#ENTRYPOINT ["/usr/bin/entrypoint.sh"]
CMD ["java", "-jar", "medical-file.jar"]